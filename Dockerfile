# Start with Ubuntu 22.04 ARM base image
FROM --platform=linux/arm64/v8 ubuntu:22.04

# Prevent interactive prompts during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Set timezone
ENV TZ=UTC

ARG BUILD_VERSION
ENV BUILD_VERSION=${BUILD_VERSION}

# Install basic requirements including Node.js
RUN apt-get update && apt-get install -y \
    curl \
    wget \
    git \
    openssh-server \
    supervisor \
    ca-certificates \
    gnupg \
    gettext \
    && mkdir -p /etc/apt/keyrings \
    && curl -fsSL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Verify Node.js and npm versions
RUN node --version && npm --version

# Create required directories
RUN mkdir -p /var/log/supervisor /var/run/sshd /app \
    && chown root:root /var/run/sshd \
    && chmod 0755 /var/run/sshd

# Set working directory
WORKDIR /app

# Create a group with GID 1001
RUN groupadd -g 1001 strapi

# Create a non-root user with UID 1001 and GID 1001
RUN useradd -r -u 1001 -m -g strapi -s /bin/bash strapi \
    && echo "strapi:strapi" | chpasswd \
    && adduser strapi sudo

# Set up SSH for strapi user
RUN mkdir -p /home/strapi/.ssh \
    && chmod 700 /home/strapi/.ssh \
    && chown strapi:users /home/strapi/.ssh

# Generate SSH host keys
RUN ssh-keygen -A

# Copy package.json and package-lock.json
COPY package*.json ./

# Copy app source and .env file
COPY . .
COPY .env.production .env

# Set ownership for the app directory
RUN chown -R strapi:users /app

# Configure SSH server
COPY sshd_config /etc/ssh/sshd_config
RUN chmod 644 /etc/ssh/sshd_config

# Copy supervisor configuration
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Install app dependencies and build as strapi user
USER strapi
RUN npm install && \
    NODE_ENV=production npm run build

# Switch back to root for supervisord
USER root

# Expose the Strapi and SSH ports
EXPOSE 1337 22

# Start supervisor
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]
