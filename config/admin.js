module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'd07f4951c9d708a19d724bd0f1cf7342'),
  },
});
