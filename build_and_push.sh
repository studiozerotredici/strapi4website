#!/bin/bash

# Set your Docker Hub username and repository name
DOCKER_HUB_USERNAME="zerotredici"
REPO_NAME="strapi.zerotredici.com"

# Generate version tag based on date and time
VERSION_TAG=$(date "+%Y%m%d_%H%M%S")

# Build the Docker image
docker build --build-arg BUILD_VERSION=$VERSION_TAG -t $DOCKER_HUB_USERNAME/$REPO_NAME:$VERSION_TAG .

# Tag the image as latest as well
docker tag $DOCKER_HUB_USERNAME/$REPO_NAME:$VERSION_TAG $DOCKER_HUB_USERNAME/$REPO_NAME:latest

# Log in to Docker Hub
echo "Logging in to Docker Hub..."
#docker login -  la variablile del token d'accesso viene letta fuori dalle variabili esportate in bashrc o bash_profile sul server ARM per la creazione delle immagini.
echo "$DOCKER_HUB_ACCESS_TOKEN" | docker login -u "$DOCKER_HUB_USERNAME" --password-stdin

# Push the images to Docker Hub
echo "Pushing images to Docker Hub..."
docker push $DOCKER_HUB_USERNAME/$REPO_NAME:$VERSION_TAG
docker push $DOCKER_HUB_USERNAME/$REPO_NAME:latest

echo "Build and push completed. Image tag: $VERSION_TAG"