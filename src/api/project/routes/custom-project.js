module.exports = {
	routes: [
		{
			method: 'GET',
			path: '/projects/category/:category',
			handler: 'project.findByCategory',
			config: {
				auth: false
			}
		}
	]
}