'use strict';

/**
 *  project controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::project.project', ({ strapi }) => ({
	/**
	 * Find a project by its id or link (extend default)
	 * @param ctx - The context object.
	 * @returns The full entity with all the data.
	 */
	async findOne(ctx) {
		const params = ctx.params

		//execute standard request if param is id
		if(parseInt(params.id) == params.id ) {
			const response = await super.findOne(ctx)
			return response
		}

		//get the id of the element with the given link
		const response = await strapi.db.query('api::project.project').findOne({ 
			where: {
				link : params.id
			}
		})

		if(!response) { return ctx.notFound(`element ${params.id} not found`, { element: params.id }) }
		
		//we must use the id of the element in the entityService (as far as I know)
		let elementId = response.id

		const fullEntity = await strapi.entityService.findOne('api::project.project', elementId, { 
			populate: {
				'cover': { 'populate':'*' },
				'categories': { 'populate':'*' },
				'awards_list': { 'populate':'*' },
				'project_gallery' : { 
					populate : { 
						'image' : true,
						'tools': true,
						'anchor_link': true,
						'elements' : { populate: { 'image' : true }},
						'portrait_el': { populate: '*' },
						'landscape_el' : { populate: '*' }
					}
				}
			}
		})

		return fullEntity
	},
	/**
	 * Find all projects of the given category
	 * @param ctx - The context object.
	 * @returns An array of objects with the following structure:
	 */
	async findByCategory(ctx) {
		const params = ctx.params

		const response = await strapi.entityService.findMany('api::project.project', { 
			fields: ['id','title','link'],
			filters: { 
				categories: { 
					slug: {
						$contains: params.category
					}
				}
			},
			populate: ['categories']
		})

		console.log(response)
		return response
	}
}));
