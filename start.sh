#!/bin/bash

cd /app
NODE_ENV=production npm start

# Create directories if they don't exist
mkdir -p /app/public/uploads

# Set correct permissions 
chown -R strapi:strapi /app/public

# Start Strapi (adjust the command if necessary)
npm run start 